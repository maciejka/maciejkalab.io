// Toggle cards

$(".courses").click(function () {
  $(this).toggleClass("is-active", "is-inactive");
});

// Progres - bars

$(".progres-bar").each((i, e) => {
  $(e).css("width", $(e).data("width") + "%");
});

let w = $(window);
let stripes = $(".skills");

let fired = false;

let scroll = () => {
  if (!fired) {
    if (w.scrollTop() + w.height() > stripes.offset().top + 50) {
      $(".progress-bar").each((i, e) => {
        $(e).css("width", $(e).data("width") + "%");
      });
      fired = true;
    }
  }
};

w.scroll(scroll);
